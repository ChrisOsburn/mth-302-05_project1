/* 
Author: Chris Osburn
The following code uses Euler's method to approximate a differential equation dy/dx=y+x-2.
The user can enter their own step sizes and initial values, as well as the y(x) they are looking for.
*/

#include<stdio.h>
#include<iostream>
#include <chrono> 
using namespace std::chrono;
using namespace std;

double diff(double x, double y)
{
    double f;
    f = y + x-2;            //Enter differential equations here, only tested with y+x-2
    return f;
}
int main()
{
        double a, b, x, y, h, t, y1;
        printf("\nEnter x0, y0, h, y(x): \n");
        printf("x0 = ");
        cin >> a;
        printf("\ny0 = ");
        cin >> b;
        printf("\nh = ");
        cin >> h;
        printf("\ny(x) = ");
        cin >> t;
        x = a;
        y = b;
        printf("\n  h\t\t  y\t\t exact\t absolute error\t\t relative error\n");
        auto start = high_resolution_clock::now();      //Begin timer
        while (x < t)
        {
            y1 = h * diff(x, y);        //Math used for Euler's Method
            y = y + y1;
            x = x + h;
            if (x > (t+0.0000000001)) {       //This resolves slight error in very small step sizes
                break;
            }
            printf("%0.10lf\t%0.10lf\t-e\t %0.10lf\t\t %0.10lf\n", x, y, (y - (-2.718281828)), (((y - (-2.718281828)) / -2.718281828) * 100));
        }
        auto stop = high_resolution_clock::now();                       //End timer
        auto duration = duration_cast<microseconds>(stop - start);
        cout <<"Calculations executed in "<< duration.count()<<" microseconds" << endl;
    }
